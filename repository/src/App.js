import logo from './logo.svg';
import React, { useState, useEffect } from 'react';
import './App.css';

function App() {
  const [ip, setIP] = useState();

  useEffect(() => {
    fetch('http://api.ipify.org/?format=json')
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      console.log(data);
      setIP(data.ip);
    });
    
  }, []);
   
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>ReactJS  APP!</h1>
        <h2>TERRAFORM</h2>
        <h2> Anton Sapanovskiy </h2>
      </header>
    </div>
  )
}
export default App; 
